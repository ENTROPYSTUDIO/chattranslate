import React, { useCallback, useLayoutEffect, useState } from 'react'
import { GiftedChat } from 'react-native-gifted-chat'
import auth from '@react-native-firebase/auth';
//import firebase from '@react-native-firebase/app';
import database from '@react-native-firebase/database';

export default function ChatScreen({navigation}){
  // Set an initializing state whilst Firebase connects
    // const [initializing, setInitializing] = useState(true);
    // const [user, setUser] = useState();
    const [messages, setMessages] = useState([]);
    // useEffect(() => {
    //     setMessages([
    //     {
    //         _id: 1,
    //         text: 'Hello developer',
    //         createdAt: new Date(),
    //         user: {
    //         _id: 2,
    //         name: 'React Native',
    //         avatar: 'https://placeimg.com/140/140/any',
    //         },
    //     },
    //     ])
    // }, [])

    const currentUser   = auth().currentUser;

    useLayoutEffect(() => {
        let ref = database().ref('chats');
        const unsubscribe = ref.orderByKey().on('value',(snapshot) => {
            if(snapshot.exists()){
                snapshot.forEach((childSnap) => {
                    let snapshotVal   = childSnap.val();
                    setMessages(previousMessages => GiftedChat.append(previousMessages,{
                        _id: snapshotVal._id,
                        createdAt: new Date(),
                        text: snapshotVal.text,
                        user: snapshotVal.user,
                    }));
                });
            }
            return ref.off('value', unsubscribe);
        });

    }, [])

    const onSend = useCallback((messages = []) => {
        setMessages(previousMessages => GiftedChat.append(previousMessages, messages))
        const {
            _id,
            createdAt,
            text,
            user,
        } = messages[0];
        console.log(messages[0]);

        database().ref('chats').push().set({
            _id,
            createdAt : Date.now(),
            text,
            user,
        })
    }, [])

    return (
        <GiftedChat
            messages={messages}
            showAvatarForEveryMessage={true}
            renderUsernameOnMessage={true}
            onSend={messages => onSend(messages)}
            user={{
                _id: currentUser.email,
                name: currentUser.displayName,
                avatar: currentUser.photoURL,
            }}
        />
    )
}